## [1.1.5](https://gitlab.com/hove-ict/tools/vaultwarden/compare/v1.1.4...v1.1.5) (2024-06-13)


### Bug Fixes

* ci updates ([b1c5a08](https://gitlab.com/hove-ict/tools/vaultwarden/commit/b1c5a089c4bd94ddcbaba1af529cf78f2fa1a349))

## [1.1.4](https://gitlab.com/hove-ict/tools/vaultwarden/compare/v1.1.3...v1.1.4) (2024-2-11)


### Bug Fixes

* default postgres hostname ([cfacb3c](https://gitlab.com/hove-ict/tools/vaultwarden/commit/cfacb3c578dabd66996585c777ea45a55eba0daa))

## [1.1.3](https://gitlab.com/hove-ict/tools/vaultwarden/compare/v1.1.2...v1.1.3) (2024-2-11)


### Bug Fixes

* add postgres host variables ([d44b5d8](https://gitlab.com/hove-ict/tools/vaultwarden/commit/d44b5d8f89a2218e108adab268bac72f91abebf6))

## [1.1.2](https://gitlab.com/hove-ict/tools/vaultwarden/compare/v1.1.1...v1.1.2) (2023-12-23)


### Bug Fixes

* new deploy ci ([7188c09](https://gitlab.com/hove-ict/tools/vaultwarden/commit/7188c09e8ccd80313ce9a36c2fc048ebc8b860f2))

## [1.1.1](https://gitlab.com/hove-ict/tools/vaultwarden/compare/v1.1.0...v1.1.1) (2023-12-23)


### Bug Fixes

* use CI_PROJECT_NAME for postgres ([2a12767](https://gitlab.com/hove-ict/tools/vaultwarden/commit/2a12767b45969e7ce732d20fcb0e2ce6cb878e2e))

# [1.1.0](https://gitlab.com/hove-ict/tools/vaultwarden/compare/v1.0.2...v1.1.0) (2023-12-23)


### Features

* trigger release ([86e7f45](https://gitlab.com/hove-ict/tools/vaultwarden/commit/86e7f4584cdeacac245949d58fb1619024375773))

## [1.0.2](https://gitlab.com/hove-ict/tools/vaultwarden/compare/v1.0.1...v1.0.2) (2023-02-21)


### Bug Fixes

* compose new url ([c999cab](https://gitlab.com/hove-ict/tools/vaultwarden/commit/c999cab146839822575535e2d497ce78ba9b1dbb))

## [1.0.1](https://gitlab.com/hove-ict/tools/vaultwarden/compare/v1.0.0...v1.0.1) (2023-02-21)


### Bug Fixes

* update postgres ([8455c2e](https://gitlab.com/hove-ict/tools/vaultwarden/commit/8455c2e472d010b85002d414512572843bb2ab87))

# 1.0.0 (2023-02-17)


### Bug Fixes

* compose url ([695cb3e](https://gitlab.com/hove-ict/tools/vaultwarden/commit/695cb3e2eb2ca486c793d12058e26af776c7e49a))


### Features

* use ci templates ([9c29fab](https://gitlab.com/hove-ict/tools/vaultwarden/commit/9c29fab5cafa9533683811a575eebee83c3f9f65))
